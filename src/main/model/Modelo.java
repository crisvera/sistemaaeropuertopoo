package main.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Esta clase describe el modelo interno de un avion (los asientos
 * y los pasillos).
 */
public class Modelo implements Serializable {
    
    private static final long serialVersionUID = 152967055567757555L;
    
    private HashMap<Integer, HashMap<Integer, Asiento>> modelo;
    private String nombre;

    public Modelo(HashMap<Integer, HashMap<Integer, Asiento>> modelo, String nombre) {
        this.modelo = modelo;
        this.nombre = nombre;
    }

    public HashMap<Integer, HashMap<Integer, Asiento>> getModelo() {
        return modelo;
    }

    public void setModelo(HashMap<Integer, HashMap<Integer, Asiento>> modelo) {
        this.modelo = modelo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
    public String getRepresentacionASCII () {
        
        StringBuilder builder = new StringBuilder();
        
        int numfilas = modelo.keySet().size();
        
        for (int i = 0; i < numfilas; i++) {
            
            int numcols = modelo.get(0).keySet().size();
            
            for (int j = 0; j < numcols; j++) {
                
                Asiento asiento = modelo.get(i).get(j);
                
                if(asiento.esDePrimeraClase()) {
                    builder.append("P");
                } else if (!asiento.esSentable()) {
                    builder.append(" ");
                } else if(!asiento.esDePrimeraClase()) {
                    builder.append("S");
                }
                
            }
            
            builder.append("\n");
        }
        
        return builder.toString();
    }
    
    
    
    /**
     * Toma este modelo de avion, recorre ordenadamente las filas y cols, y luego
     * imprime la letra que representa cada asiento corretamente a la consola.
     * <p>
     * El proposito de este metodo es para poder comprobar que los modelos que hacemos
     * parse de un archivo de texto si se hayan formateado bien.
     */
    public void imprimirModeloEnConsolaEnASCII() {
        
        System.out.println("Probando modelo");
        
        int numfilas = modelo.keySet().size();
        
        for (int i = 0; i < numfilas; i++) {
            
            int numcols = modelo.get(0).keySet().size();
            
            for (int j = 0; j < numcols; j++) {
                
                Asiento asiento = modelo.get(i).get(j);
                
                if(asiento.esDePrimeraClase()) {
                    System.out.print("P");
                } else if (!asiento.esSentable()) {
                    System.out.print(" ");
                } else if(!asiento.esDePrimeraClase()) {
                    System.out.print("S");
                }
                
            }
            
            System.out.println("");
        }
    }

    
    public Modelo deepClone() {
        
        HashMap<Integer, HashMap<Integer, Asiento>> modeloAPasar = new HashMap<>();
        
        int filas = modelo.keySet().size();
        
        for (int fila = 0; fila < filas; fila++) {
            
            HashMap<Integer, Asiento> asientosMap = new HashMap<>();
            
            int numAsientosEnLaFila = modelo.get(fila).keySet().size();
            
            for (int col = 0; col < numAsientosEnLaFila; col++) {
                
                Asiento asiento = modelo.get(fila).get(col).deepClone();
                
                asientosMap.put(col, asiento);
            }
            
            modeloAPasar.put(fila, asientosMap);
        }
        
        return new Modelo(modeloAPasar, nombre);
    }
    
    
}
