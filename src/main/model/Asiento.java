package main.model;

import java.io.Serializable;

/**
 * Clase que representa un asiento fisico del avion. Puede ser
 * de primer clase o segunda. Puede tener una silla o no (si no
 * tiene es porque es un pasillo).
 */
public class Asiento implements Serializable {
    
    private static final long serialVersionUID = 1529699999127757698L;
    
    // Si no es de primera clase, es de segunda
    private final boolean esDePrimeraClase;
    // Si no tiene silla, es un pasillo
    private final boolean esSentable;
    // Estar reservado es decir que ya se vendio
    private boolean esYaVendido;
    
    private int fila;
    private int col;

    public Asiento(int fila, int col, boolean esDePrimeraClase, boolean esSentable, boolean esYaVendido) {
        this.fila = fila;
        this.col = col;
        this.esDePrimeraClase = esDePrimeraClase;
        this.esSentable = esSentable;
        this.esYaVendido = esYaVendido;
    }

    public boolean esDePrimeraClase() {
        return esDePrimeraClase;
    }

    public boolean esSentable() {
        return esSentable;
    }

    public boolean esYaVendido() {
        return esYaVendido;
    }

    public void setEsYaVendido(boolean esYaVendido) {
        this.esYaVendido = esYaVendido;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public Asiento deepClone () {
        return new Asiento(Integer.valueOf(fila), Integer.valueOf(col),
                Boolean.valueOf(esDePrimeraClase), Boolean.valueOf(esSentable), Boolean.valueOf(esYaVendido));
    }

    @Override
    public String toString() {
        return "Asiento{" + "esDePrimeraClase=" + esDePrimeraClase + ", esSentable=" + esSentable + ", esYaVendido=" + esYaVendido + ", fila=" + fila + ", col=" + col + '}';
    }

    
    
    
    
}
