package main.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import javafx.util.StringConverter;

/**
 * Esta clase toma una instancia de modelo de avion del sistema y la convierte a
 * un string.
 */
public class ModeloStringConverter extends StringConverter<HashMap<Integer, HashMap<Integer, Asiento>>> {

    private HashMap<String, HashMap<Integer, HashMap<Integer, Asiento>>> mapaDeModelos = new HashMap<String, HashMap<Integer, HashMap<Integer, Asiento>>>();

    public ModeloStringConverter(HashMap<Integer, HashMap<Integer, Asiento>> modelo1,
            HashMap<Integer, HashMap<Integer, Asiento>> modelo2) {
        mapaDeModelos.put("modelA1", modelo1);
        mapaDeModelos.put("modelB1", modelo2);
    }

    @Override
    public String toString(HashMap<Integer, HashMap<Integer, Asiento>> modelo) {

        for (HashMap<Integer, HashMap<Integer, Asiento>> modeloEnLaLista : mapaDeModelos.values()) {
            if (modelo == modeloEnLaLista) {
                return getKeyByValue(mapaDeModelos, modelo);
            }
        }

        return "Error!";
    }

    @Override
    public HashMap<Integer, HashMap<Integer, Asiento>> fromString(String nombreDelModelo) {
        return mapaDeModelos.get(nombreDelModelo);
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

}
