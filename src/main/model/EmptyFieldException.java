package main.model;

/**
 * Se lanza cuando un campo esta vacio
 */
public class EmptyFieldException extends Exception {
    
    public EmptyFieldException () {
        super("Existe al menos un campo vacio!");
    }
}
