package main.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;


/**
 * Clase que representa un vuelo
 */
public class Vuelo implements Serializable {
    
    private static final long serialVersionUID = 1529685098267751234L;

    private String codigo_de_vuelo;
    private CodigoIATA codigo_IATA_arribo;
    private CodigoIATA codigo_IATA_salida;
    private LocalDate fecha_de_embarque, fecha_de_salida, fecha_de_arribo;
    private LocalTime hora_de_embarque, hora_de_salida, hora_de_arribo;
    private String puerta_de_arribo, puerta_de_salida;
    private Modelo asientos_disponibles;
    
    public Vuelo() {}
    
    private Vuelo (Builder builder) {
        
        codigo_de_vuelo = builder.codigo_de_vuelo;
        codigo_IATA_arribo = builder.codigo_IATA_arribo;
        codigo_IATA_salida = builder.codigo_IATA_salida;
        fecha_de_embarque = builder.fecha_de_embarque;
        fecha_de_salida = builder.fecha_de_salida;
        fecha_de_arribo = builder.fecha_de_arribo;
        hora_de_embarque = builder.hora_de_embarque;
        hora_de_salida = builder.hora_de_salida;
        hora_de_arribo = builder.hora_de_arribo;
        puerta_de_arribo = builder.puerta_de_arribo;
        puerta_de_salida = builder.puerta_de_salida;
        asientos_disponibles = builder.asientos_disponibles;
        
    }

    public String getCodigo_de_vuelo() {
        return codigo_de_vuelo;
    }

    public void setCodigo_de_vuelo(String codigo_de_vuelo) {
        this.codigo_de_vuelo = codigo_de_vuelo;
    }

    public CodigoIATA getCodigo_IATA_arribo() {
        return codigo_IATA_arribo;
    }

    public void setCodigo_IATA_arribo(CodigoIATA codigo_IATA_arribo) {
        this.codigo_IATA_arribo = codigo_IATA_arribo;
    }

    public CodigoIATA getCodigo_IATA_salida() {
        return codigo_IATA_salida;
    }

    public void setCodigo_IATA_salida(CodigoIATA codigo_IATA_salida) {
        this.codigo_IATA_salida = codigo_IATA_salida;
    }

    public LocalDate getFecha_de_embarque() {
        return fecha_de_embarque;
    }

    public void setFecha_de_embarque(LocalDate fecha_de_embarque) {
        this.fecha_de_embarque = fecha_de_embarque;
    }

    public LocalDate getFecha_de_salida() {
        return fecha_de_salida;
    }

    public void setFecha_de_salida(LocalDate fecha_de_salida) {
        this.fecha_de_salida = fecha_de_salida;
    }

    public LocalDate getFecha_de_arribo() {
        return fecha_de_arribo;
    }

    public void setFecha_de_arribo(LocalDate fecha_de_arribo) {
        this.fecha_de_arribo = fecha_de_arribo;
    }

    public LocalTime getHora_de_embarque() {
        return hora_de_embarque;
    }

    public void setHora_de_embarque(LocalTime hora_de_embarque) {
        this.hora_de_embarque = hora_de_embarque;
    }

    public LocalTime getHora_de_salida() {
        return hora_de_salida;
    }

    public void setHora_de_salida(LocalTime hora_de_salida) {
        this.hora_de_salida = hora_de_salida;
    }

    public LocalTime getHora_de_arribo() {
        return hora_de_arribo;
    }

    public void setHora_de_arribo(LocalTime hora_de_arribo) {
        this.hora_de_arribo = hora_de_arribo;
    }

    public String getPuerta_de_arribo() {
        return puerta_de_arribo;
    }

    public void setPuerta_de_arribo(String puerta_de_arribo) {
        this.puerta_de_arribo = puerta_de_arribo;
    }

    public String getPuerta_de_salida() {
        return puerta_de_salida;
    }

    public void setPuerta_de_salida(String puerta_de_salida) {
        this.puerta_de_salida = puerta_de_salida;
    }
    
    public Modelo getAsientos_disponibles() {
        return asientos_disponibles;
    }

    public void setAsientos_disponibles(Modelo asientos_disponibles) {
        this.asientos_disponibles = asientos_disponibles;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.codigo_de_vuelo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vuelo other = (Vuelo) obj;
        if (!Objects.equals(this.codigo_de_vuelo, other.codigo_de_vuelo)) {
            return false;
        }
        return true;
    }
    
    public String getVueloEnFormatoDashboard () {
        return String.format("%s de %s a %s embarque %s %s llegada %s %s", 
                codigo_de_vuelo, codigo_IATA_salida, codigo_IATA_arribo,
                fecha_de_embarque, hora_de_embarque,
                fecha_de_arribo, hora_de_arribo);
    }
    
    @Override
    public String toString() {
        return String.format("[Vuelo] codigo: %s%n"
                + "sale de: %s llega a: %s%n"
                + "embarque: %s %s%n"
                + "salida: %s %s%n"
                + "llegada: %s %s%n"
                + "puerta salida: %s puerta llegada: %s%n"
                + "modelo: %s%n", 
                codigo_de_vuelo, 
                codigo_IATA_salida, codigo_IATA_arribo, 
                fecha_de_embarque, hora_de_embarque,
                fecha_de_salida, hora_de_salida,
                fecha_de_arribo, hora_de_arribo,
                puerta_de_salida, puerta_de_arribo,
                asientos_disponibles);
    }
    
    public static class Builder {
        
        private String codigo_de_vuelo;
        private CodigoIATA codigo_IATA_arribo;
        private CodigoIATA codigo_IATA_salida;
        private LocalDate fecha_de_embarque, fecha_de_salida, fecha_de_arribo;
        private LocalTime hora_de_embarque, hora_de_salida, hora_de_arribo;
        private String puerta_de_arribo, puerta_de_salida;
        private Modelo asientos_disponibles;
        
        public Builder() {}

//        public Builder(String codigo_de_vuelo, CodigoIATA codigo_IATA_arribo, CodigoIATA codigo_IATA_salida, LocalDate fecha_de_embarque, LocalDate fecha_de_salida, LocalDate fecha_de_arribo, LocalTime hora_de_embarque, LocalTime hora_de_salida, LocalTime hora_de_arribo, String puerta_de_arribo, String puerta_de_salida, String categorias, String cadena, String lugar_de_arribo, Modelo asientos_disponibles) {
//            this.codigo_de_vuelo = codigo_de_vuelo;
//            this.codigo_IATA_arribo = codigo_IATA_arribo;
//            this.codigo_IATA_salida = codigo_IATA_salida;
//            this.fecha_de_embarque = fecha_de_embarque;
//            this.fecha_de_salida = fecha_de_salida;
//            this.fecha_de_arribo = fecha_de_arribo;
//            this.hora_de_embarque = hora_de_embarque;
//            this.hora_de_salida = hora_de_salida;
//            this.hora_de_arribo = hora_de_arribo;
//            this.puerta_de_arribo = puerta_de_arribo;
//            this.puerta_de_salida = puerta_de_salida;
//            this.categorias = categorias;
//            this.cadena = cadena;
//            this.lugar_de_arribo = lugar_de_arribo;
//            this.asientos_disponibles = asientos_disponibles;
//        }
        
        public Builder codigo_de_vuelo(String val) {
            codigo_de_vuelo = val;
            return this;
        }
        
        public Builder codigo_IATA_arribo(CodigoIATA val) {
            codigo_IATA_arribo = val;
            return this;
        }
        
        public Builder codigo_IATA_salida(CodigoIATA val) {
            codigo_IATA_salida = val;
            return this;
        }
        
        public Builder fecha_de_embarque(LocalDate val) {
            fecha_de_embarque = val;
            return this;
        }
        
        public Builder fecha_de_salida(LocalDate val) {
            fecha_de_salida = val;
            return this;
        }
        
        public Builder fecha_de_arribo(LocalDate val) {
            fecha_de_arribo = val;
            return this;
        }
        
        public Builder hora_de_embarque (LocalTime val) {
            hora_de_embarque = val;
            return this;
        }
        
        
        public Builder hora_de_salida (LocalTime val) {
            hora_de_salida = val;
            return this;
        }
        
        
        public Builder hora_de_arribo (LocalTime val) {
            hora_de_arribo = val;
            return this;
        }
                
        public Builder puerta_de_arribo (String val) {
            puerta_de_arribo = val;
            return this;
        }
        
        public Builder puerta_de_salida (String val) {
            puerta_de_salida = val;
            return this;
        }
        
        public Builder asientos_disponibles (Modelo val) {
            asientos_disponibles = val;
            return this;
        }
        
        public Vuelo build() {
            return new Vuelo(this);
        }
        
    }

}
