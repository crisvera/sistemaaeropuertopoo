package main.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase temporal de transicion entre el almacenamiento en archivos txt vs
 * almacenamiento a traves de la serializacion.
 * <p>
 * Contiene varias funciones utilitarias de lectura y procesamiento de archivos.
 */
public class ProcesadorDeArchivos {
    
    private static int contador = 0;
    
    private ProcesadorDeArchivos() {}
    
    /**
     * De-serializa un archivo binario para generar un objeto Sistema.
     * 
     * @return el sistema mas recientemente grabado 
     */
    public static Sistema cargarSistemaDeSerializandoArchivoBinario() {
        
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("sistema.bin"));) {
            System.out.println("Se va a cargar el sistema a traves de la de-serializacion de un archivo binario");
            return (Sistema) objectInputStream.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProcesadorDeArchivos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ProcesadorDeArchivos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * Lee una lista csv con la informacion de todos los empleados y la usa para
     * obtener su informacion y crear los apropiadas objetos.
     *
     * @return un ArrayList con todos los objetos empleado.
     */
    public static ArrayList<Empleado> cargarEmpleados() {

        ArrayList<Empleado> empleados = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"))) {
            // Salta primera linea porque no contiene info relevante
            br.readLine();

            String linea;
            while ((linea = br.readLine()) != null) {
                empleados.add(procesaEmpleadoDesdeStringCSV(linea));
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        return empleados;
    }
    
    /**
     * De una linea de texto obtenida en un archivo con todos los empleados en
     * formato CSV, separa los campos para generar el empleado apropiado
     *
     * @param linea un String con valores separados por coma
     * @return un objeto empleado
     */
    public static Empleado procesaEmpleadoDesdeStringCSV(String linea) {

        String cedula, nombre, apellido, email, usuario, clave, rol, aerolinea, departamento;
        Empleado empleado;

        ArrayList<String> valores = new ArrayList<>(Arrays.asList(linea.split(",")));

        cedula = valores.get(0);
        nombre = valores.get(1);
        apellido = valores.get(2);
        email = valores.get(3);
        usuario = valores.get(4);
        clave = valores.get(5);
        rol = valores.get(6).toUpperCase();
        aerolinea = valores.get(7);
        departamento = valores.get(8);

        switch (rol) {
            case "A":
                empleado = new Administrador(cedula, email, departamento, usuario, clave, rol);
                break;
            case "C":
                // aqui hay un error. Encontrar la forma de obtener objetos Aerolinea apropiados
                empleado = new Cajero(cedula, email, departamento, usuario, clave, new Aerolinea(aerolinea), rol);
                break;
            case "P":
                empleado = new Planificador(cedula, email, departamento, usuario, clave, rol,new Aerolinea(aerolinea));
                break;
            default:
                empleado = null;
        }

        return empleado;

    }
    
    public static ArrayList<Aerolinea> cargarAerolineas() {
        ArrayList<Aerolinea> aerolineas = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader("aerolineas.txt"))) {
            // Salta primera linea porque no contiene info relevante
            br.readLine();

            String linea;
            while ((linea = br.readLine()) != null) {
                aerolineas.add(procesaAerolineaDesdeStringCSV(linea));
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        return aerolineas;
    }
    
    /**
     * De una linea de texto obtenida en un archivo con todas las aerolineas en
     * formato CSV, separa los campos para generar las aerolineas apropiadas
     *
     * @param linea un String con valores separados por coma
     * @return un objeto aerolinea
     */
    private static Aerolinea procesaAerolineaDesdeStringCSV(String linea) {
        String nombre = linea;
        return new Aerolinea(nombre);
    }
    
    public static Sistema inicializarSistemaDesdeTxt(){
        
        Sistema s = new Sistema();
        s.actualizarDatosLeyendoTexto();
        contador++;
        System.out.println("Se cargo el sistema leyendo texto");
        System.out.println("Veces que se inicializa el sistema usando la funcion "
        + "inicializarSistemaDesdeTxt(): " + contador);

        s.guardarSistema();
        return s;
    }
    
    /**
     * De un archivo de texto que contiene el modelo de un avion
     * (segun como lo envia el prof) lo convierte a una instancia
     * de modelo de avion en formato:
     * <p>
     * Hashmap<Integer,Haspmap<Integer,Asiento>>
     *<p>
     * Esto representa el layout del modelo del avion. 
     *<p>
     * P = Primera clase
     * <p>
     * S = Segunda clase
     * <p>
     * Un espacio representa el pasillo.
     * <p>
     * 
     * En resumen, esta funcion retorna un map con todos los asientos
     * ordernado de tal manera que las claves coordinen. Si desdeon el
     * asiento {0,0} solo necesito llamar modelo.get(0).get(0).
     * 
     * @param nombreArchivoConExtension es el nombre del archivo que contiene el formato
     * @param nombreDelModelo
     * @return 
     */
    public static Modelo parseModeloAvion (String nombreArchivoConExtension, String nombreDelModelo) {
        
        try(BufferedReader br = new BufferedReader(new FileReader(nombreArchivoConExtension))) {
            
            final String glifoPrimeraClase = "P";
            final String glifoSegundaClase = "S";
            
            // Este modelo representa una matriz que contiene asientos
            // el puesto {0,0} representa el asiento de la parte mas arriba
            // en la parte mas izq. {1,1} entonces representa un asiento una fila
            // mas abajo, y una col a la derecha.
            HashMap<Integer, HashMap<Integer, Asiento>> modelo = new HashMap<>();
            
            int filaActual = 0;      
            
            // Recorro todas las lineas
            String linea;
            while ((linea = br.readLine()) != null) {
                
                // Este map representa todos los asientos de la fila actual
                HashMap<Integer,Asiento> asientosEnLaFilaActual = new HashMap<>();
                
                // Estos booleans me sirviran para crear el correcto objeto asiento
                boolean esDePrimeraClase;
                boolean esSentable;
                // Tengo un array con todas las lineas del .txt con el formato de modelo
                char[] charArrayDeLinea = linea.toCharArray();
                
                // Recorro el array
                // Este for representa el trabajo que se realiza en la fila actual
                // en su totalidad. Una vez que termina el for, todos los asientos de
                // el modelo han sido procesados y el map que los contiene esta completo
                for (int i = 0; i < charArrayDeLinea.length; i++) {
                    // representa cada letra del array
                    String letra = String.valueOf(charArrayDeLinea[i]);
                    // Dependiendo de la letra instancio los booleans
                    switch(letra) {
                        case glifoPrimeraClase:
                            esDePrimeraClase = true;
                            esSentable = true;
                            break;
                        case glifoSegundaClase:
                            esDePrimeraClase = false;
                            esSentable = true;
                            break;
                        // Si no es de primera ni segunda es un pasillo
                        default:
                            esDePrimeraClase = false;
                            esSentable = false;
                    }
                    
                    // Una vez que tengo lo booleans, instancio un objeto asiento
                    // como estoy creando un modelo nuevo, ningun asiento esta 
                    // vendido en este momento
                    Asiento asiento = new Asiento(filaActual, i, esDePrimeraClase, esSentable, false);
                    
                    // Asigno el asiento en la posicion correcta de la columna
                    asientosEnLaFilaActual.put(i, asiento);
                    
                }
                
                // En este punto todavia sigo trabajando en la fila
                
                // Una vez que tengo el map con todos los asientos de la fila
                // lo agrego al map general y actualizo la fila actual antes de ir a la siguiente
                modelo.put(filaActual, asientosEnLaFilaActual);
                filaActual++;
                
            }
            
            // Aqui ya recorre todas la lineas y tengo un map con todo
            return new Modelo(modelo, nombreDelModelo);
            
            
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        return null;
    }
    

}
