package main.model;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 */
public class DashboardRunnableVueloMovedor implements Runnable {

    private ArrayList<Vuelo> vuelos;
    private ArrayList<Label> labelsDeVuelos;
    private Thread t;
    private final String NOMBRE_THREAD = "thread de mover vuelos";
    
    public DashboardRunnableVueloMovedor (ArrayList<Vuelo> vuelos, ArrayList<Label> labelsDeVuelos) {
        this.vuelos=vuelos;
        this.labelsDeVuelos=labelsDeVuelos;
    }
    
    @Override
    public void run() {
        
        int labelActualIdx = 0;
        int vueloActualIdx = 0;
        final int VUELOS_SIZE = vuelos.size();
        
        while(true) {
            
            Label labelActual = labelsDeVuelos.get(labelActualIdx);
            Vuelo vueloActual = vuelos.get(vueloActualIdx);
            
            
            // este bloque se necesita porque los elementos UI de la
            // aplicacion solo se puede correr desde el main thread
            Platform.runLater(new Runnable(){

                @Override
                public void run() {
                    labelActual.setText(vueloActual.getVueloEnFormatoDashboard());
                }
            });
            
            
            
            if (labelActualIdx == 2) {
                labelActualIdx = 0;
            } else {
                labelActualIdx++;
            }
            
            if (vueloActualIdx == (VUELOS_SIZE - 1) ) {
                vueloActualIdx = 0;
            } else {
                vueloActualIdx++;
            }
            
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DashboardRunnableVueloMovedor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void start () {
        System.out.println("Se llamo start() en " + NOMBRE_THREAD);
        if(t == null) {
            t = new Thread(this, NOMBRE_THREAD);
            t.start();
        }
    }
    
}
