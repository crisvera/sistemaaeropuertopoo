package main.model;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 */
public class DashboardRunnableAvionMovedor implements Runnable {
    

    private ArrayList<Avion> aviones;
    private ArrayList<Label> labelsDeAviones;
    private Thread t;
    private final String NOMBRE_THREAD = "thread de mover aviones";

    public DashboardRunnableAvionMovedor(ArrayList<Avion> aviones, ArrayList<Label> labelsDeAviones) {
        this.aviones = aviones;
        this.labelsDeAviones = labelsDeAviones;
    }
    

    @Override
    public void run() {
        
        int labelActualIdx = 0;
        int avionActualIdx = 0;
        final int AVIONES_SIZE = aviones.size();
        
        while(true) {
            
            Label labelActual = labelsDeAviones.get(labelActualIdx);
            Avion avionActual = aviones.get(avionActualIdx);
            
            
            // este bloque se necesita porque los elementos UI de la
            // aplicacion solo se puede correr desde el main thread
            Platform.runLater(new Runnable(){

                @Override
                public void run() {
                    labelActual.setText(avionActual.toString() + 
                            "Distancia max: " + avionActual.getDistancia_maxima() +
                            "Fabricante: " + avionActual.getFabricante());
                }
            });
            
            
            
            if (labelActualIdx == 2) {
                labelActualIdx = 0;
            } else {
                labelActualIdx++;
            }
            
            if (avionActualIdx == (AVIONES_SIZE - 1) ) {
                avionActualIdx = 0;
            } else {
                avionActualIdx++;
            }
            
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DashboardRunnableVueloMovedor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void start () {
        System.out.println("Se llamo start() en " + NOMBRE_THREAD);
        if(t == null) {
            t = new Thread(this, NOMBRE_THREAD);
            t.start();
        }
    }
    

}
