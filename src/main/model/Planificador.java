package main.model;

/**
 *
 */
public class Planificador extends Empleado {

    public Aerolinea aerolinea;

    private static final long serialVersionUID = 1123685098267757698L;
    
    public Planificador(String identificador, String correo, String departamento, String usuario, String clave, String rol, Aerolinea aerolinea) {
        super(identificador, correo, departamento, usuario, clave, rol);
        //se debe crear un metodo que llene los datos de la aerolinea
        this.aerolinea = aerolinea;
    }

    public Aerolinea getAerolinea() {
        return aerolinea;
    }
    
    
}
