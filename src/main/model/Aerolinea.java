package main.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Clase que representra una aerolinea.
 */
public class Aerolinea implements Serializable {
    
    private static final long serialVersionUID = 1529685055557757698L;
    public String nombre;
    public ArrayList<Avion> lista_de_aviones;
    public ArrayList<Vuelo> vuelos;
    public HashMap<Integer, Avion> gates;

    public Aerolinea(String nombre) {
        this.nombre = nombre;
        lista_de_aviones = new ArrayList();
        vuelos = new ArrayList();
    }

    public ArrayList<Avion> getLista_de_aviones() {
        return lista_de_aviones;
    }

    public ArrayList<Vuelo> getVuelos() {
        return vuelos;
    }
    
    public void agregar_avion(Avion avion) {
        lista_de_aviones.add(avion);
    }

    public void agregar_vuelo(Vuelo vuelo) {
        vuelos.add(vuelo);
    }

    public boolean verificar_vuelo(Vuelo vuelo) {
        return false;
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
    
}
