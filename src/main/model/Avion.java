package main.model;

import java.io.Serializable;

/**
 * Clase que representa un avion
 */
public class Avion implements Serializable {
    
    private static final long serialVersionUID = 4206985098267757698L;
    
    private final int numero_de_serie;
    private final double distancia_maxima;
    private final String fabricante;
    private final Modelo modelo;

    public Avion(int numero_de_serie, double distancia_maxima, String fabricante, Modelo modelo) {
        this.numero_de_serie = numero_de_serie;
        this.distancia_maxima = distancia_maxima;
        this.fabricante = fabricante;
        this.modelo = modelo;
    }

    public int getNumero_de_serie() {
        return numero_de_serie;
    }

    public double getDistancia_maxima() {
        return distancia_maxima;
    }

    public String getFabricante() {
        return fabricante;
    }

    public Modelo getModelo() {
        return modelo;
    }

    @Override
    public String toString() {
        return String.valueOf(numero_de_serie + " - " + modelo);
    }
}
