package main.model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * El sistema es la clase que contiene la informacion del programa. Esto incluye
 * pero no se limita a: lista de empleados, aerolineas, viajes.
 */
public class Sistema implements Serializable {

    private static final long serialVersionUID = 1529685098267757777L;
    
    private ArrayList<Empleado> empleados;
    private ArrayList<Aerolinea> aerolineas;
    private ArrayList<Modelo> modelosDeAvion;
    private Empleado empleadoActual;
    private  Aerolinea aerolineaActual;
    private ArrayList<Venta> ventas;

    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(ArrayList<Venta> ventas) {
        this.ventas = ventas;
    }
    
    
    
    public void setEmpleadoActual(Empleado empleadoActual) {
        this.empleadoActual = empleadoActual;
    }

    public Empleado getEmpleadoActual() {
        return empleadoActual;
    }
    public Aerolinea getAerolineaActual(Empleado empleado){
            if (empleadoActual instanceof Cajero) {
                Cajero c =  (Cajero) empleadoActual;
                return c.getAerolinea();
            }
            if (empleadoActual instanceof Planificador) {
                Planificador p =  (Planificador) empleadoActual;
                
                return p.getAerolinea();
            }
            return null;
        }
    
  

    /**
     * Valida el login.
     * 
     * @param usr
     * @param clave es la clave que el usuario ingresa por pantalla, sin encriptar
     * @return 
     */
    public boolean esLoginValido(String usr, String clave) {

        clave = SeguridadFX.encriptar(clave);

        for (Empleado empleado : empleados) {

            if (empleado.getUsuario().equalsIgnoreCase(usr)
                    && empleado.getClave().equals(clave)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param usr es el texto que representa el username de un empleado
     * @return un empleado en la lista de empleados con el mismo usuario, nulo
     * de lo contrario
     */
    public Empleado encuentraEmpleadoSegunUsuario(String usr) {
        
        for (Empleado empleado : empleados) {
            
            if (empleado.getUsuario().equals(usr))
                return empleado;
        }
        
        return null;
    }
    
    public void agregarNuevoEmpleadoALista(Empleado empleado) {
        
        empleados.add(empleado);
    }
    
    public void agregaNuevaAerolineaALista (Aerolinea a) {
        
        this.aerolineas.add(a);
    }
    
    public ArrayList<Aerolinea> getAerolineas() {
        
        return aerolineas;
    }

    public ArrayList<Modelo> getModelosDeAvion() {
        return modelosDeAvion;
    }

    public void setModelosDeAvion(ArrayList<Modelo> modelosDeAvion) {
        this.modelosDeAvion = modelosDeAvion;
    }

    public Aerolinea getAerolineaActual() {
        return aerolineaActual;
    }

    public void setAerolineaActual(Aerolinea aerolineaActual) {
        this.aerolineaActual = aerolineaActual;
    }
    
    
    
    /**
     * Usa serializacion para guardar este objeto en un archivo binario. 
     */
    public void guardarSistema() {

        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("sistema.bin"))) {
            
            Sistema s = new Sistema();
            s.setAerolineas(this.aerolineas);
            s.setEmpleados(this.empleados);
            s.setEmpleadoActual(empleadoActual);
            s.setModelosDeAvion(modelosDeAvion);
            s.setAerolineaActual(aerolineaActual);
            s.setVentas(ventas);
            
            objectOutputStream.writeObject(s);
            objectOutputStream.flush();
            System.out.println("Se guardo el sistema por serializacion");
            
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public void setEmpleados(ArrayList<Empleado> empleados) {
        this.empleados = empleados;
    }

    public void setAerolineas(ArrayList<Aerolinea> aerolineas) {
        this.aerolineas = aerolineas;
    }

    public ArrayList<Empleado> getEmpleados() {
        return empleados;
    }
    
    // NO BORRAR!!!!!!
    public void actualizarDatosLeyendoTexto() {

        empleados = ProcesadorDeArchivos.cargarEmpleados();
        aerolineas = ProcesadorDeArchivos.cargarAerolineas();
    }
    
    public void printDebugInfo() {
        System.out.println("\nSe va imprimir la informacion del sistema:");
        System.out.println("----------------------------------------------");
        System.out.println("----------------------------------------------");
//        imprimirEmpleadorAConsola();
//        imprimirAerolineasAConsola();
        imprimirVentasAconsola();
//        imprimitAsientosDeModeloAConsola();
        System.out.println("----------------------------------------------");
        System.out.println("----------------------------------------------");
    }
    
    private void imprimitAsientosDeModeloAConsola () {
        
        System.out.println("SE  VAN A IMPRIMIR TODOS LOS ASIENTOS");
        
        for( Aerolinea a : this.getAerolineas()) {
            
            System.out.println(a.getNombre());
            
            for (Vuelo v: a.getVuelos()) {
                
                System.out.println(v);
                
                Modelo m = v.getAsientos_disponibles();
                
                HashMap<Integer, HashMap<Integer, Asiento>> modelo = m.getModelo();

                int filas = modelo.keySet().size();

                for (int fila = 0; fila < filas; fila++) {

                    HashMap<Integer, Asiento> asientosMap = new HashMap<>();

                    int numAsientosEnLaFila = modelo.get(fila).keySet().size();

                    for (int col = 0; col < numAsientosEnLaFila; col++) {

                        Asiento asiento = modelo.get(fila).get(col);

                        System.out.println(asiento.toString());
                    }

//                    modelo.put(fila, asientosMap);
                }
            }
        }
    }
    
    private void imprimirVentasAconsola() {
        
        System.out.println("imprmiendo ventas");
        
        for(Venta v: this.ventas) {
            System.out.println(v);
        }
    }
    
    private void imprimirEmpleadorAConsola(){

        for (Empleado empleado : this.empleados) {
            System.out.println(empleado);
        }
    }
   
    private void imprimirAerolineasAConsola() {
        
        for (Aerolinea aerolinea : this.aerolineas) {
            
            System.out.println(String.format("\nAerolinea [%s] con los siguiente aviones:\n", aerolinea.getNombre()));
            
            for (Avion avion : aerolinea.getLista_de_aviones()) {
                
                System.out.println(String.format("Avion. Serie: %s", avion.getNumero_de_serie()));
                System.out.println("Modelo: " + avion.getModelo());
                avion.getModelo().imprimirModeloEnConsolaEnASCII();
                
            }
            
            System.out.println(String.format("\nAerolinea [%s] con los siguientes vuelos:\n", aerolinea.getNombre()));
            imprimirVuelosAConsola(aerolinea);
        }
    }
    
    private void imprimirVuelosAConsola (Aerolinea a) {
        
        for (Vuelo v : a.getVuelos()) {
            System.out.println(v);
        }
        
//                Vuelo v = new Vuelo.Builder().codigo_de_vuelo("1").codigo_IATA_arribo(CodigoIATA.GYE)
//                .codigo_IATA_salida(CodigoIATA.GYE).fecha_de_embarque(LocalDate.MAX)
//                .fecha_de_arribo(LocalDate.MAX).fecha_de_salida(LocalDate.MAX)
//                .hora_de_arribo(LocalTime.MAX).hora_de_embarque(LocalTime.MAX).hora_de_salida(LocalTime.MAX)
//                .puerta_de_arribo("").puerta_de_salida("").categorias("cat").cadena("cadena").lugar_de_arribo("")
//                .asientos_disponibles(this.getModelosDeAvion().get(0)).build();
//                
//                System.out.println(v);
    }
    
}
