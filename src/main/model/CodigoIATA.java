package main.model;

/**
 * Representa los codigos IATA incluyendo la ciudad y el
 * nombre del aeropuerto.
 */
public enum CodigoIATA {
    
    GYE ("Guayaquil", "Jose Joaquin de Olmedo in Guayaquil"),
    UIO ("Quito", "Mariscal Sucre International Airport"),
    LIM ("Lima", "Aeropuerto Internacional Jorge Chavez"),
    BOG ("Bogota", "El dorado Internacional"),
    MIA ("Miami", "Miami International Airport");
    
    private final String ciudad;
    private final String aeropuerto;

    private CodigoIATA(String ciudad, String aeropuerto) {
        this.ciudad = ciudad;
        this.aeropuerto = aeropuerto;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getAeropuerto() {
        return aeropuerto;
    }

    @Override
    public String toString() {
        return ciudad;
    }
    
    
    
}
