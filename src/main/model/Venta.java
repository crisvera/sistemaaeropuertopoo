package main.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 */
public class Venta implements Serializable {
    
    private static final long serialVersionUID = 1529441555557757198L;

    private ArrayList<Asiento> asientosVendidos;
    private Vuelo vuelo;
    private double totalValor;

    public Venta(ArrayList<Asiento> asientosVendidos, Vuelo vuelo) {
        this.asientosVendidos = asientosVendidos;
        this.vuelo = vuelo;
        totalValor=0 ;
        
        for(Asiento a: asientosVendidos){
            
            if( !a.esDePrimeraClase() ){
                totalValor+=600;
            }else{
                totalValor+=2000;
            }
            
        }
        totalValor = totalValor + ( totalValor*0.12);
    }

    public ArrayList<Asiento> getAsientosVendidos() {
        return asientosVendidos;
    }

    public void setAsientosVendidos(ArrayList<Asiento> asientosVendidos) {
        this.asientosVendidos = asientosVendidos;
    }

    public Vuelo getVuelo() {
        return vuelo;
    }

    public void setVuelo(Vuelo vuelo) {
        this.vuelo = vuelo;
    }

    public double getTotalValor() {
        return totalValor;
    }

    public void setTotalValor(double totalValor) {
        this.totalValor = totalValor;
    }

    @Override
    public String toString() {
        
        String miString = "";
        StringBuilder sb = new StringBuilder();
        
        for (Asiento a: asientosVendidos) {
            
            sb.append(a.toString());
            sb.append("\n");
        }
        
        sb.append(String.format("Valor total:  " + this.totalValor));
        
        miString = sb.toString();
        
        return miString;
    }
    
    
}
