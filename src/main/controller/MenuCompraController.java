/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.model.Asiento;
import main.model.SentMails;
import main.model.Venta;
import main.model.Vuelo;

/**
 * FXML Controller class
 *
 * @author acast
 */
public class MenuCompraController extends ControladorAbstracto implements Initializable {

    /**
     * Initializes the controller class.
     */
    String[] lineas;
    
    private ArrayList<Asiento> asientosVendidos;
    private Vuelo vuelo;
    
    @FXML
    private Label usr_msg;
    @FXML
    private TextField correo_lbl;
    @FXML
    private TextField nombre_lbl;
    @FXML
    private Label aero_msg;
    @FXML
    private Label toast_msg;
    @FXML
    private void HandleSig(ActionEvent a){
        try{
           if(MenuCajeroVentaController.asientosPorVender.size()>=1){
            Asiento asiento = MenuCajeroVentaController.asientosPorVender.get(0);
            
//            SentMails.send(correo_lbl.getText(), "compra", nombre_lbl.getText()+"\n"+asiento.toString(),"castropooespol@gmail.com", "1234Contrasen@4321");
            
            toast_msg.setText(nombre_lbl.getText()+"\n"+asiento.toString());
            asientosVendidos.add(asiento);
            MenuCajeroVentaController.asientosPorVender.remove(0);
           }else{
               Venta v = new Venta(asientosVendidos, vuelo);
               sistema.getVentas().add(v);
               sistema.guardarSistema();
               
               //llevar a menu 
               cambiarVentanaA("MenuCajeroFXML.fxml", a);
           }
        }catch(Exception e){
            
            toast_msg.setText("erorr es: " + e.getMessage());
            
        }
        
        
    } 
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usr_msg, aero_msg);
        vuelo = MenuCajeroVentaController.vueloSeleccionado;
        asientosVendidos = new ArrayList<>();
       
    }    
    
}
