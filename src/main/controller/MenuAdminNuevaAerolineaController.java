package main.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.model.Aerolinea;

/**
 * FXML Controller class
 */
public class MenuAdminNuevaAerolineaController extends ControladorAbstracto implements Initializable {
    
    @FXML
    private TextField nombre_fld;
    
    @FXML
    private Label msg;
    
    @FXML
    private Label usr_msg;
    
    /**
     * Crea una nueva aerolinea y la agrega al sistema
     * @param event 
     */
    @FXML
    private void manejaCrearAccion (ActionEvent event) {
        
        if(nombre_fld.getText() == null || nombre_fld.getText().isEmpty()) {
            
            msg.setText(msg.getText() + "Tiene que ingresar un nombre!");
            
        } else {
            
            String nombre = nombre_fld.getText();

            Aerolinea a = new Aerolinea(nombre);

            sistema.agregaNuevaAerolineaALista(a);

            sistema.guardarSistema();

            msg.setText("Aerolinea: " + a.getNombre() + " creada con exito!");
        }
            
        

        
    }
    
    @FXML
    private void manejaRegresarAccion (ActionEvent event) {
        cambiarVentanaA("MenuAdminFXML.fxml", event);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usr_msg, null);
    }    
    
}
