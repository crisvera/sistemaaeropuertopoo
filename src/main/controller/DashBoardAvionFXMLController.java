/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import main.model.Aerolinea;
import main.model.Avion;
import main.model.DashboardRunnableAvionMovedor;
import main.model.DashboardRunnableVueloMovedor;

/**
 * FXML Controller class
 *
 * @author zen
 */
public class DashBoardAvionFXMLController extends ControladorAbstracto implements Initializable {

    private ArrayList<Avion> aviones = new ArrayList<>();
    
    @FXML
    private Label a1_lbl;
    
    @FXML
    private Label a2_lbl;
    
    @FXML
    private Label a3_lbl;
    

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        for(Aerolinea a : sistema.getAerolineas()) {
            for (Avion avion: a.getLista_de_aviones()) {
                aviones.add(avion);
            }
        }
        
        
        ArrayList<Label> labels = new ArrayList<>();
        labels.add(a1_lbl);
        labels.add(a2_lbl);
        labels.add(a3_lbl);
       
        
        DashboardRunnableAvionMovedor drvm = new DashboardRunnableAvionMovedor(aviones, labels);
        drvm.start();
        
    }    
    
}
