package main.controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.TextField;
import main.model.Avion;
import main.model.CodigoIATA;
import main.model.EmptyFieldException;
import main.model.Modelo;
import main.model.Planificador;
import main.model.Vuelo;

/**
 * FXML Controller class
 */
public class MenuPlanificadorPlanVueloController extends ControladorAbstracto implements Initializable {

    @FXML
    private Label usr_label;
    
    @FXML
    private Label aerolinea_label;
    
    @FXML
    private TextField cod_vuelo_field;
    
    @FXML
    private ComboBox IATA_salida_cbox;
    
    @FXML
    private ComboBox IATA_arribo_cbox;
    
    @FXML
    private DatePicker embarque_datepicker;
    
    @FXML
    private Spinner embarque_hora_spinner;
    
    @FXML
    private DatePicker salida_datepicker;
    
    @FXML
    private Spinner salida_hora_spinner;
    
    @FXML
    private DatePicker arribo_datepicker;
    
    @FXML
    private Spinner arribo_hora_spinner;
    
    @FXML
    private ComboBox avion_cbox;
    
    @FXML
    private TextField puerta_arribo_fld;
    
    @FXML
    private TextField puerta_salida_fld;
    
    @FXML
    private Label toast_label;
    
    @FXML
    private void manejaCrearAccion(ActionEvent event) {
        
        String codigo_de_vuelo;
        CodigoIATA codigo_IATA_arribo;
        CodigoIATA codigo_IATA_salida;
        LocalDate fecha_de_embarque, fecha_de_salida, fecha_de_arribo;
        LocalTime hora_de_embarque, hora_de_salida, hora_de_arribo;
        String puerta_de_arribo, puerta_de_salida;
        Modelo asientos_disponibles;
        
        try {
            
            if (
                    cod_vuelo_field.getText().isEmpty() ||
                    IATA_salida_cbox.getSelectionModel().isEmpty() ||
                    IATA_arribo_cbox.getSelectionModel().isEmpty() ||
                    avion_cbox.getSelectionModel().isEmpty() ||
                    puerta_arribo_fld.getText().isEmpty() ||
                    puerta_salida_fld.getText().isEmpty() ||
                    embarque_datepicker.getValue() == null ||
                    salida_datepicker.getValue() == null ||
                    arribo_datepicker.getValue() == null ) {
                
                throw new EmptyFieldException();
            }
            
            codigo_de_vuelo = cod_vuelo_field.getText();
            codigo_IATA_salida = (CodigoIATA) IATA_salida_cbox.getValue();
            codigo_IATA_arribo = (CodigoIATA) IATA_arribo_cbox.getValue();
            
            Avion avionSeleccionado = (Avion) avion_cbox.getValue();
            asientos_disponibles = avionSeleccionado.getModelo().deepClone();
            
            puerta_de_arribo = puerta_arribo_fld.getText();
            puerta_de_salida = puerta_salida_fld.getText();
            
            fecha_de_embarque = embarque_datepicker.getValue();
            int horaEmbarqueSpinnerVal = (Integer)embarque_hora_spinner.getValue();
            hora_de_embarque = LocalTime.of(horaEmbarqueSpinnerVal,0);
            
            fecha_de_salida = salida_datepicker.getValue();
            int horaSalidaSpinnerVal = (Integer) salida_hora_spinner.getValue();
            hora_de_salida = LocalTime.of(horaSalidaSpinnerVal, 0);
            
            fecha_de_arribo = arribo_datepicker.getValue();
            int horaArriboSpinnerVal = (Integer) arribo_hora_spinner.getValue();
            hora_de_arribo = LocalTime.of(horaArriboSpinnerVal, 0);

            Vuelo vuelo = new Vuelo.Builder().
                    codigo_de_vuelo(codigo_de_vuelo).
                    codigo_IATA_arribo(codigo_IATA_arribo).codigo_IATA_salida(codigo_IATA_salida).
                    fecha_de_embarque(fecha_de_embarque).
                    fecha_de_salida(fecha_de_salida).
                    fecha_de_arribo(fecha_de_arribo).
                    hora_de_embarque(hora_de_embarque).
                    hora_de_salida(hora_de_salida).
                    hora_de_arribo(hora_de_arribo).
                    puerta_de_arribo(puerta_de_arribo).
                    puerta_de_salida(puerta_de_salida).
                    asientos_disponibles(asientos_disponibles).
                    build();
            
            Planificador planificadorActual = (Planificador) sistema.getEmpleadoActual();
            planificadorActual.getAerolinea().getVuelos().add(vuelo);
            sistema.guardarSistema();
            toast_label.setText(String.format("Vuelo %s creado con exito!", vuelo.getCodigo_de_vuelo()));
      
        } catch (EmptyFieldException e) {
            toast_label.setText(e.getMessage());
        }
        
    }
    
    @FXML
    private void manejaRegresarAccion (ActionEvent event) {
        cambiarVentanaA("MenuPlanificadorFXML.fxml", event);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usr_label, aerolinea_label);
        
        // Inicializa los spinners para solo recorran de 0 a 23 (24 es el fin del dia)
        IntegerSpinnerValueFactory intFactory0 = new IntegerSpinnerValueFactory(0, 23);
        IntegerSpinnerValueFactory intFactory1 = new IntegerSpinnerValueFactory(0, 23);
        IntegerSpinnerValueFactory intFactory2 = new IntegerSpinnerValueFactory(0, 23);
        embarque_hora_spinner.setValueFactory(intFactory0);
        salida_hora_spinner.setValueFactory(intFactory1);
        arribo_hora_spinner.setValueFactory(intFactory2);
        
        // Inicializa las comboboxes
        IATA_salida_cbox.getItems().addAll(
                CodigoIATA.GYE, CodigoIATA.UIO, CodigoIATA.LIM, CodigoIATA.BOG, CodigoIATA.MIA);
        IATA_arribo_cbox.getItems().addAll(
                CodigoIATA.GYE, CodigoIATA.UIO, CodigoIATA.LIM, CodigoIATA.BOG, CodigoIATA.MIA);
        
        // Inicializa el combobox de aviones
        Planificador planificadorActual = (Planificador) sistema.getEmpleadoActual();
        avion_cbox.getItems().addAll(planificadorActual.getAerolinea().getLista_de_aviones());
    }    
    
}
