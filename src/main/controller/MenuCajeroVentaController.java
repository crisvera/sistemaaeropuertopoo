package main.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import main.model.Asiento;
import main.model.Cajero;
import main.model.EmptyFieldException;
import main.model.Vuelo;

/**
 * FXML Controller class
 */
public class MenuCajeroVentaController extends ControladorAbstracto implements Initializable {

    public static Vuelo vueloSeleccionado;
    public static ArrayList<Asiento> asientosPorVender = new ArrayList<>();
    
    private ImageViewClickHandler ivHandler;
    
    @FXML
    private Label user_msg;
    
    @FXML
    private Label aerolinea_msg;
    
    @FXML
    private ComboBox vuelos_cbox;
    
    @FXML
    private Label toast_lbl;
    
    @FXML
    private VBox reserva_vbox;
    
    /**
     * Cuando el usuario selecciona uno de los vuelos, si este
     * no es nullo (si es que en realidad selecciona algo) se usa
     * ese vuelo para popular las celdas del GridPane
     * 
     * @param event 
     */
    @FXML
    private void manejaPopulaGridPaneConSeleccionAccion (ActionEvent event) {
    
        try {
            
            if (vuelos_cbox.getSelectionModel().isEmpty()) {
                throw new EmptyFieldException();
            }
            
            Vuelo vuelo = (Vuelo) vuelos_cbox.getValue();
            
            Image image;

            GridPane reserva_asientos_gridpane = new GridPane();
            reserva_vbox.getChildren().add(reserva_asientos_gridpane);
            
            HashMap<Integer,HashMap<Integer,Asiento>> asientosMap = vuelo.getAsientos_disponibles().getModelo();
            int numFilas = asientosMap.keySet().size();
            
            for (int fila = 0; fila < numFilas; fila++) {
                
                HashMap<Integer,Asiento> asientosEnFilaActualMap = asientosMap.get(fila);
                
                int numCols = asientosEnFilaActualMap.keySet().size();
                
                for (int col = 0; col < numCols; col++) {
                    
                    Asiento asiento = asientosMap.get(fila).get(col);
                    
                    image = getImageSegunAsiento(asiento);
                    ImageView imageView = new ImageView(image);
                    
                    // Para que se puedan reservar asientos le asigno click handlers
                    // a los image views para que puedan procesar ese trabajo pero 
                    // solo lo hago cuando se les asigna un asiento que aun no esta reservado
                    // pues el usuario no puede des-reservar un asiento que ya ha
                    // sido reservado previamente
                    if (!asiento.esYaVendido() && asiento.esSentable()) {
                        ivHandler = new ImageViewClickHandler(imageView, asientosMap);
                        imageView.setOnMouseClicked(ivHandler);
                        
                    }
                    
                    
                    reserva_asientos_gridpane.add(imageView, col, fila);
                    
                }
                
                
            }
            
        } catch (EmptyFieldException ex) {
            System.out.println(ex.getMessage());
            toast_lbl.setText("Error en manejaPopulaGridPaneConSeleccionAccion: " + ex);
        }
        
            
    }
    
    @FXML
    private void manejaContinuarAccion (ActionEvent event) {
        
        // Toma el vuelo seleccionado y actualiza en la variable que va 
        // a usar el siguiente controlador
        try {
            
            if (vuelos_cbox.getSelectionModel().isEmpty())
                throw new EmptyFieldException();
            
            vueloSeleccionado = (Vuelo) vuelos_cbox.getValue();
        } catch (EmptyFieldException e) {
            toast_lbl.setText("Debe seleccionar al menos un vuelo!");
        }
        
        // cambiar ventana a la siguiente
        cambiarVentanaA("MenuCompra.fxml", event);
    }
    
    @FXML
    private void manejaRegresarAccion (ActionEvent event) {
        cambiarVentanaA("MenuCajeroFXML.fxml", event);
    }
    
    /**
     * Obtiene el apropiado objeto Image segun el tipo de asiento
     * 
     * @param a
     * @return 
     */
    private Image getImageSegunAsiento (Asiento a) {
            
        Image image = null;
        
        String urlRecursoImagen = getUrlRecursoImgAsientoSegunAsiento(a);
        
        try(FileInputStream fis = new FileInputStream(urlRecursoImagen)) {

            image = new Image(fis);

        } catch (FileNotFoundException ex) {
            toast_lbl.setText("Error en getImageSegunAsiento: " + ex);
        } catch (IOException ex) {
            toast_lbl.setText("Error en getImageSegunAsiento: " + ex);
        }
            
        return image;
    }
    
    /**
     * Obtiene el apropiado objeto Image segun el tipo de asiento.
     * <p>
     * Esta version de este metodo se usa si el usuario desea "invertir"
     * la imagen, i.e. desea hacer que un asiento no reservado se convierta
     * en una que si esta reservado.
     * 
     * @param a
     * @return 
     */
    private Image getImageSegunAsiento (Asiento a, boolean deseaInvertir) {
            
        Image image = null;
        
        String urlRecursoImagen;
        
        if (deseaInvertir) {
            urlRecursoImagen = getUrlRecursoAlReservarInvertir(a);
        } else
            urlRecursoImagen = getUrlRecursoImgAsientoSegunAsiento(a);
        
        try(FileInputStream fis = new FileInputStream(urlRecursoImagen)) {

            image = new Image(fis);

        } catch (FileNotFoundException ex) {
            toast_lbl.setText("Error en getImageSegunAsiento: " + ex);
        } catch (IOException ex) {
            toast_lbl.setText("Error en getImageSegunAsiento: " + ex);
        }
            
        return image;
    }
    
    
    
    /**
     * Dependiendo del tipo de asiento retorna un String con el nombre
     * del archivo jpg que representa ese asiento.
     * 
     * @param a es el asiento
     * @return un String con el nombre del archivo jpg
     */
    private String getUrlRecursoImgAsientoSegunAsiento (Asiento a) {
        
        String econ_libre = "econ_libre.jpg";
        String eco_reservado = "econ_reservado.jpg";
        String negocio_libre = "negocio_libre.jpg";
        String negocio_reservado = "negocio_reservado.png";
        String pasillo = "pasillo.png";
        
        if (!a.esSentable())
            return pasillo;
        else if (a.esDePrimeraClase() && !a.esYaVendido())
            return negocio_libre;
        else if (!a.esDePrimeraClase() && a.esYaVendido())
            return eco_reservado;
        else if (!a.esDePrimeraClase() && !a.esYaVendido())
            return econ_libre;
        else if (a.esDePrimeraClase() && a.esYaVendido())
            return negocio_reservado;
        else {
            toast_lbl.setText("Error en getUrlRecursoImgAsientoSegunAsiento");
            return null;
        }
        
    }
    
    /**
     * Cuando el usuario da click a un asiento, si no esta reservado,
     * entonces lo reserva. Solo lo puede reservar si aun no ha sido
     * reservado. Este metodo obtiene la version jpg del asiento que 
     * se va a reservar.
     * 
     * @param a
     * @return el url de la imagen con version reservada del asiento.
     * ya ha sido vendido, returna la imagen del asiento que deberia ser
     * normalmente.
     */
    private String getUrlRecursoAlReservarInvertir (Asiento a) {
        
        String econ_libre = "econ_libre.jpg";
        String eco_reservado = "econ_reservado.jpg";
        String negocio_libre = "negocio_libre.jpg";
        String negocio_reservado = "negocio_reservado.png";
        String pasillo = "pasillo.png";
        
        if (!a.esYaVendido()) {
            
            if (a.esDePrimeraClase())
                return negocio_reservado;
            else
                return eco_reservado;
        }
        else
            return getUrlRecursoImgAsientoSegunAsiento(a);
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Para poder ver los titulos (username, aerolinea) en la ventana
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), user_msg, aerolinea_msg);
        
        // Por si acaso limpio las lista de los valores a pasar
        asientosPorVender.clear();
        
        // Inicializa vuelos_cbox para que contenga todos los vuelos
        // de la aerolinea actual
        // Obtengo el cajero actual para extraer de el la aerolinea, y de ella los vuelo
        Cajero cajeroActual = (Cajero) sistema.getEmpleadoActual();
        vuelos_cbox.getItems().addAll(cajeroActual.getAerolinea().getVuelos());
                
    }    
    
    /**
     * Esta clase privada maneja lo que ocurre cuando el usuario da clic a un
     * asiento del GridPane (lo reserva). Este handler asume que el ImageView
     * que lo recibe ya ha sido validado (no se debe asignar este handler a un
     * ImageView que representa un asiento ya reservado pues un usuario no puede
     * des-reservar un asiento que ya ha sido reservado previamente).
     */
    private class ImageViewClickHandler implements EventHandler<MouseEvent> {
        
        private final ImageView ivFuente;
        private final HashMap<Integer,HashMap<Integer,Asiento>> asientosMap;

        public ImageViewClickHandler(ImageView ivFuente, HashMap<Integer,HashMap<Integer,Asiento>> asientosMap) {
            this.ivFuente = ivFuente;
            this.asientosMap = asientosMap;
        }
        
        @Override
        public void handle (MouseEvent event) {
        
            int fila = GridPane.getRowIndex(ivFuente);
            int col = GridPane.getColumnIndex(ivFuente);
            
            // Una vez que tengo la fila y la col del imageview que contiene el asiento
            // al cual el usr dio click, uso esa misma fila y col para obtener el asiento
            // en esa fila y col equivalente en el asientos map
            Asiento asientoQueDieronClick = asientosMap.get(fila).get(col);
            
            // Una vez que tengo esto cambio la imagen para que ahora se vea reservado y
            // lo agrega a la lista de asientos que se van a vender en esta sesion
            Image image = ivFuente.getImage();
            image = getImageSegunAsiento(asientoQueDieronClick, true);
            ivFuente.setImage(image);
            asientoQueDieronClick.setEsYaVendido(true);
            sistema.guardarSistema();
            asientosPorVender.add(asientoQueDieronClick);
            
        }
    }
    
}
