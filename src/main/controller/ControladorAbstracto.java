package main.controller;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import main.model.Cajero;
import main.model.Empleado;
import main.model.Planificador;
import main.model.ProcesadorDeArchivos;
import main.model.Sistema;

/**
 * Clase abstracta para compartir funcionalidad en general entre los
 * diferentes controladores.
 */
public abstract class ControladorAbstracto {
    
    protected Sistema sistema = ProcesadorDeArchivos.cargarSistemaDeSerializandoArchivoBinario();
    
    /**
     * Metodo utilitario para poder cambiar de ventanas mas facil.
     * 
     * @param archivoFXML es el nombre del archivo fxml que contiene el layout
     * @param event 
     */
    protected void cambiarVentanaA (String archivoFXML, ActionEvent event) {
        
        // Obtiene la ventana actual
        Stage ventanaDeDondeVieneLaFuenteDelClick = (Stage) ((Node) event.getSource()).getScene().getWindow();
        // Obtiene la ventana a donde quiero ir
        Parent root = obtenerRootSegunRecursoFXML(archivoFXML);
        // Cambio la ventana
        ventanaDeDondeVieneLaFuenteDelClick.setScene(new Scene(root));
    }
    
    /**
     * Metodo utilitario para poder obtener layouts inflados mas facil.
     * 
     * @param nombreArchivoFXML es el nombre del archivo fxml que contiene el layout
     * 
     * @return el layout inflado 
     */
    protected Parent obtenerRootSegunRecursoFXML (String nombreArchivoFXML) {
        
        String urlDelRecurso = "main/resources/" + nombreArchivoFXML;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource(urlDelRecurso));
        } catch (IOException ex) {
            System.out.println("\nError de acceso de recurso\n " + ex);
        }
        return root;
    }
    
    protected void inicializarTitulosConEmpleadoActual (Empleado empleadoActual, Label usernameLbl, Label aeroLabel) {
        
        // Si aeroLabel es null, entonces estamos tratando con un Admin
        if (aeroLabel == null) {
            usernameLbl.setText("Usuario: " + empleadoActual.getUsuario());
        } else {
            if (empleadoActual instanceof Cajero) {
                Cajero c =  (Cajero) empleadoActual;
                usernameLbl.setText("Usuario: " + c.getUsuario());
                aeroLabel.setText("Aerolinea: " + c.getAerolinea());
            }
            if (empleadoActual instanceof Planificador) {
                Planificador p =  (Planificador) empleadoActual;
                usernameLbl.setText("Usuario: " + p.getUsuario());
                aeroLabel.setText("Aerolinea: " + p.getAerolinea());
            }
        }
        
        
    }
    
}
