package main.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.model.Empleado;
import main.model.Modelo;
import main.model.ProcesadorDeArchivos;
import main.model.Venta;

/**
 * FXML Controller class
 */
public class MenuLoginController extends ControladorAbstracto implements Initializable {

    @FXML
    private TextField usuario;

    @FXML
    private PasswordField clave;
    
    /**
     * Si el usuario presiona enter despues de escribir la clave, el
     * programa lo trata como si llamara al btn login.
     * 
     * @param event 
     */
    @FXML
    private void manejaEnterDesdeClaveFieldAccion (ActionEvent event) {
        
        handleLoginAction(event);
    }

    @FXML
    private void handleLoginAction(ActionEvent event) {

        String usuarioString = usuario.getText();
        String claveString = clave.getText();

        if (sistema.esLoginValido(usuarioString, claveString)) {
            
            // Es login es valido
            Empleado empleado = sistema.encuentraEmpleadoSegunUsuario(usuarioString);
            sistema.setEmpleadoActual(empleado);
            sistema.guardarSistema();
            
            // Abrir una segunda ventana con los vuelos
            Stage ventana2 = new Stage();
            ventana2.setTitle("DashBoard de Vuelos");
            ventana2.setScene(new Scene(obtenerRootSegunRecursoFXML("DashBoardFXML.fxml")));
            ventana2.show();
            
            Stage ventana3 = new Stage();
            ventana3.setTitle("DashBoard de Vuelos");
            ventana3.setScene(new Scene(obtenerRootSegunRecursoFXML("DashBoardAvionFXML.fxml")));
            ventana3.show();
            
            
            // Dependiendo del tipo de empleado muestra el menu que le toca
            switch (empleado.getRol()) {
                case "A":
                    cambiarVentanaA("MenuAdminFXML.fxml", event);
                    break;
                case "C":
                    cambiarVentanaA("MenuCajeroFXML.fxml", event);
                    
                    break;
                case "P":
                    cambiarVentanaA("MenuPlanificadorFXML.fxml", event);
                    break;
                }
        }

    }   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sistema.printDebugInfo();
    }

}
