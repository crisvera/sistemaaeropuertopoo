package main.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import main.model.Asiento;
import main.model.Vuelo;

/**
 * FXML Controller class
 *
 */
public class MenuCajeroController extends ControladorAbstracto implements Initializable {

    @FXML
    private Label usuario_msg;
    
    @FXML
    private Label aero_msg;
    
    @FXML
    private ListView listalv;
    
    @FXML
    private void manejaVentaAccion (ActionEvent event) {
        cambiarVentanaA("MenuCajeroVentaFXML.fxml", event);
    }
    
    @FXML
    private void manejaLogoutAccion (ActionEvent event) {
        cambiarVentanaA("MenuLoginFXML.fxml", event);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Para poder ver los titulos (username, aerolinea) en la ventana
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usuario_msg, aero_msg);
        llenar_listview();
    }    
    
    private void llenar_listview(){
        
        ObservableList<Vuelo> myObservableList = FXCollections.observableList(sistema.getAerolineaActual(sistema.getEmpleadoActual()).vuelos);
        listalv.setItems(myObservableList);
         
        listalv.setCellFactory(new Callback<ListView<Vuelo>, ListCell<Vuelo>>(){
 
            @Override
            public ListCell<Vuelo> call(ListView<Vuelo> p) {
                 
                ListCell<Vuelo> cell = new ListCell<Vuelo>(){
 
                    @Override
                    protected void updateItem(Vuelo t, boolean bln) {
                        super.updateItem(t, bln);
                        if (t != null) {
                            setText(t.toString());
                        }
                    }
 
                };
                 
                return cell;
            }
        });
        }
    }
    

