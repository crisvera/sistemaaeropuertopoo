package main.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import main.model.Aerolinea;
import main.model.DashboardRunnableVueloMovedor;
import main.model.Vuelo;

/**
 * FXML Controller class
 *
 */
public class DashBoardFXMLController extends ControladorAbstracto implements Initializable {

    @FXML
    private Label v1_lbl;
    
    @FXML
    private Label v2_lbl;
    
    @FXML
    private Label v3_lbl;
    
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        ArrayList<Aerolinea> aerolineas = sistema.getAerolineas();
        ArrayList<Vuelo> vuelos = new ArrayList<>();
        
        for (Aerolinea aerolinea : aerolineas) {
            for (Vuelo vuelo : aerolinea.getVuelos()) {
                vuelos.add(vuelo);
            }
        }
        
        ArrayList<Label> labels = new ArrayList<>();
        labels.add(v1_lbl);
        labels.add(v2_lbl);
        labels.add(v3_lbl);
        
        // ESTE CODIGO es temporal. sirve para que al principio no sea vea
        // feo
        v2_lbl.setText(vuelos.get(vuelos.size() -1 ).getVueloEnFormatoDashboard());
        v3_lbl.setText(vuelos.get(vuelos.size() -2 ).getVueloEnFormatoDashboard());
        
        DashboardRunnableVueloMovedor drvm = new DashboardRunnableVueloMovedor(vuelos, labels);
        drvm.start();
    }    
    
}
