package main.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 */
public class MenuPlanificadorController extends ControladorAbstracto implements Initializable {

    @FXML
    private Label usr_msg;
    
    @FXML
    private Label aero_msg;
    
    @FXML
    private void manejaLogoutAccion (ActionEvent event) {
        cambiarVentanaA("MenuLoginFXML.fxml", event);
    }
    
    /**
     * Lleva al user a la ventana de planear vuelos
     * 
     * @param event 
     */
    @FXML
    private void manejaPlanearVueloAccion (ActionEvent event) {
        cambiarVentanaA("MenuPlanificadorPlanVueloFXML.fxml", event);
    }
    
    /**
     * Lleva al user a la ventana de ingresar nuevo avion
     * 
     * @param event 
     */
    @FXML
    private void manejaIngresarAvionAccion (ActionEvent event) {
        cambiarVentanaA("MenuPlanificadorIngresarAvionFXML.fxml", event);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Para poder ver los titulos (username, aerolinea) en la ventana
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usr_msg, aero_msg);
        
    }    
    
}
