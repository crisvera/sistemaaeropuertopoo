package main.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import main.model.Administrador;

/**
 * FXML Controller class
 */
public class MenuAdminController extends ControladorAbstracto implements Initializable {

    @FXML
    private Label usr_msg;
    
    @FXML
    private void manejaNuevoUsuarioAccion(ActionEvent event) {

        cambiarVentanaA("MenuAdminCrearUsuarioFXML.fxml", event);
    }

    /**
     * Al dar click se listan todos los empleados del sistema a la consola.
     * 
     * @param event 
     */
    @FXML
    private void manejaListaEmpleadosAccion(ActionEvent event) {
        System.out.println("Empleado actual: " + sistema.getEmpleadoActual().getUsuario());
        sistema.printDebugInfo();
    } 
    
    @FXML
    private void manejaCrearNuevaAerolineaAccion (ActionEvent event) {

        cambiarVentanaA("MenuAdminNuevaAerolineaFXML.fxml", event);
    }
    
    @FXML
    private void manejaLogout (ActionEvent event) {
        
        cambiarVentanaA("MenuLoginFXML.fxml", event);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usr_msg, null);
    }

}
