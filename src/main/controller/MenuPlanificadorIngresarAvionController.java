package main.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import main.model.Avion;
import main.model.EmptyFieldException;
import main.model.Modelo;
import main.model.Planificador;

/**
 * FXML Controller class
 */
public class MenuPlanificadorIngresarAvionController extends ControladorAbstracto implements Initializable {

    @FXML
    private TextField num_serie_fld;
            
    @FXML
    private TextField fabricante_fld;
            
    @FXML
    private TextField dist_max_fld;
    
    @FXML
    private ComboBox modelo_cbox;
            
    @FXML
    private Label usr_label;
    
    @FXML
    private Label aero_label;
    
    @FXML
    private Label toast_label;
    
    @FXML
    private Label modelo_ascii_label;
    
    /**
     * Cuando el usuario selecciona un modelo lo puede ver por pantalla
     * 
     * @param event 
     */
    @FXML
    private void manejaCboxAccion (ActionEvent event) {
        
        if(!modelo_cbox.getSelectionModel().isEmpty()) {
            
            Modelo modelo = (Modelo) modelo_cbox.getValue();
            modelo_ascii_label.setText(modelo.getRepresentacionASCII());
        }
    }
    
    /**
     * Al dar click en el boton crear, se va a crear un nuevo avion, y
     * se lo agrega a la aerolinea en la que trabaja el planificador usando
     * el programa.
     * 
     * @param event 
     */
    @FXML
    private void manejaCrearAccion (ActionEvent event ) {
        
        int serie;
        String fabricante;
        double distMax;
        Modelo modelo;
        
        try {
            
            if(num_serie_fld.getText().isEmpty() || 
                    fabricante_fld.getText().isEmpty() ||
                    dist_max_fld.getText().isEmpty() ||
                    modelo_cbox.getSelectionModel().isEmpty())
                throw new EmptyFieldException();
            
            serie = Integer.parseInt(num_serie_fld.getText());
            fabricante = fabricante_fld.getText();
            distMax = Double.parseDouble(dist_max_fld.getText());
            modelo = (Modelo) modelo_cbox.getValue();
            
            // Una vez que tengo todos los datos los pongo en la lista de aviones de
            // la aerolinea del planificador actual
            Planificador planificadorActual = (Planificador) sistema.getEmpleadoActual();
            Avion avion = new Avion(serie, distMax, fabricante, modelo);
            planificadorActual.getAerolinea().agregar_avion(avion);
            sistema.guardarSistema();
            toast_label.setText("Avion guardado en aerolinea " + planificadorActual.getAerolinea());
        } catch (NumberFormatException e) {
            toast_label.setText("Error en el formato de los numeros!");
        } catch (NullPointerException e) {
            toast_label.setText("Null pointer! Todos los campos deben ser llenados!");
        } catch (EmptyFieldException e) {
            toast_label.setText(e.getMessage());
        }
        
    }
    
    @FXML
    private void manejaRegresarAccion (ActionEvent event) {
        cambiarVentanaA("MenuPlanificadorFXML.fxml", event);
    }
            
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Para poder ver los titulos (username, aerolinea) en la ventana
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usr_label, aero_label);
        
        // Inicializo el combobox
        ArrayList<Modelo> modelos = sistema.getModelosDeAvion();
        
        for (Modelo modelo : modelos) {
            modelo_cbox.getItems().add(modelo);
        }
    }    
    
}
