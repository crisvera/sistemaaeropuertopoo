package main.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.model.Administrador;
import main.model.Aerolinea;
import main.model.Cajero;
import main.model.Empleado;
import main.model.Planificador;
import main.model.SeguridadFX;

/**
 * FXML Controller class
 */
public class MenuAdminCrearUsuarioController extends ControladorAbstracto implements Initializable {

    @FXML
    private TextField cedulaFld;

    @FXML
    private TextField emailFld;

    @FXML
    private TextField nombreFld;

    @FXML
    private TextField apellidoFld;

    @FXML
    private TextField usrFld;

    @FXML
    private PasswordField clavePassFld;

    @FXML
    private ComboBox rolCbox;

    @FXML
    private ComboBox aerolineaCbox;

    @FXML
    private Label msg_label;
    
    @FXML
    private Label usr_msg;
    
    @FXML
    private void manejaRegresarAccion (ActionEvent event) {
        cambiarVentanaA("MenuAdminFXML.fxml", event);
    }

    @FXML
    private void manejaCrearUsuarioAction(ActionEvent event) {

        String cedula = cedulaFld.getText();
        String email = emailFld.getText();
        String nombre = nombreFld.getText();
        String apellido = apellidoFld.getText();
        String usuario = usrFld.getText();
        String clave = clavePassFld.getText();
        String rol = (String) rolCbox.getValue();
        String departamento = Administrador.asignaDepartamentoSegunRol(rol);
        Aerolinea aerolinea = null;
        // Solo asigna la aerolinea si no es un admin
        if (!rol.equals("A")) {
            aerolinea = (Aerolinea) aerolineaCbox.getValue();
        }

        boolean datosSonValidos = false;
        
        if (!Administrador.esCedulaValida(cedula))
            msg_label.setText("La cedula no es valida!" );
        if (!Administrador.esContraseñaValida(clave))
            msg_label.setText(msg_label.getText() + " La clave no es valida!");
        
        if (Administrador.esCedulaValida(cedula)
                && Administrador.esContraseñaValida(clave)) {
            datosSonValidos = true;
            // La clave se guarda encriptada
            clave = SeguridadFX.encriptar(clave);
        }

        Empleado e = null;

        if (datosSonValidos) {

            switch (rol) {
                case "A":
                    e = new Administrador(cedula, email, departamento, usuario, clave, rol);
                    break;
                case "C":
                    e = new Cajero(cedula, email, departamento, usuario, clave, aerolinea, rol);
                    break;
                case "P":
                    e = new Planificador(cedula, email, departamento, usuario, clave, rol, aerolinea);
                    break;
            }

            sistema.agregarNuevoEmpleadoALista(e);
            msg_label.setText("Empleado creado: " + e.getUsuario());
            
            sistema.guardarSistema();
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // Para mostrar el username
        inicializarTitulosConEmpleadoActual(sistema.getEmpleadoActual(), usr_msg, null);
        
        // Inicializa los roles manualmente
        rolCbox.getItems().removeAll(rolCbox.getItems());
        rolCbox.getItems().addAll("A", "C", "P");
        rolCbox.getSelectionModel().select("A");

        // Inicializa las aerolineas dinamicamente
        ArrayList<Aerolinea> aerolineas = sistema.getAerolineas();
        for (Aerolinea aerolineaDeLista : aerolineas) {
            aerolineaCbox.getItems().add(aerolineaDeLista);
        }

    }

}
